package org.opentele.server.provider.api

enum ErrorCode {
    NOT_FOUND('notFound'),
    INVALID('invalid')

    String value

    public ErrorCode(String value) {
        this.value = value
    }

    @Override
    public String toString() {
        return value
    }
}
