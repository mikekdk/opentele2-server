package org.opentele.server.provider.model
import grails.buildtestdata.MockErrors
import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.validation.ValidationException
import org.opentele.server.core.command.NumericThresholdCommand
import org.opentele.server.core.model.types.MeasurementTypeName
import org.opentele.server.model.*
import org.opentele.server.provider.StandardThresholdSetService
import org.opentele.server.provider.ThresholdService
import spock.lang.Specification

@TestFor(ThresholdController)
@Build([NumericThreshold, PatientGroup, MeasurementType, StandardThresholdSet, Threshold])
@Mock([NumericThreshold, PatientGroup, MeasurementType, StandardThresholdSet, Threshold])
class ThresholdControllerSpec extends Specification {

    NumericThreshold numericThreshold
    PatientGroup patientGroup
    StandardThresholdSet standardThresholdSet

    NumericThresholdCommand command

    def setup() {
        controller.thresholdService = Mock(ThresholdService)
        controller.standardThresholdSetService = Mock(StandardThresholdSetService)
        command = new NumericThresholdCommand()
        numericThreshold = NumericThreshold.build(type: new MeasurementType(MeasurementTypeName.HEMOGLOBIN)).save(failOnError: true)
        Threshold.metaClass.static.get = { Long id ->
            return NumericThreshold.get(id)
        }

        patientGroup = PatientGroup.build()
        standardThresholdSet = StandardThresholdSet.build(patientGroup: patientGroup)
        session.lastController = "lastController"
        session.lastAction = "lastAction"
    }

    @SuppressWarnings("GroovyAssignabilityCheck")
    def "when editing an unknown threshold expect a redirect to last controller and action with a not found message"() {
        when:
        controller.edit()

        then:
        0 * controller.thresholdService.getThresholdCommandForEdit(null)
        0 * controller.standardThresholdSetService.findStandardThresholdSetForThreshold(_ as Threshold)

        flash.message == "default.not.found.message"
        response.redirectedUrl == '/lastController/lastAction'
    }

    @SuppressWarnings("GroovyAssignabilityCheck")
    def "when editing an existing threshold expect the edit view to be rendered with the correct model"() {
        given:
        params.id = numericThreshold.id

        when:
        controller.edit()

        then:
        1 * controller.thresholdService.getThresholdCommandForEdit(_) >> command
        1 * controller.standardThresholdSetService.findStandardThresholdSetForThreshold(numericThreshold) >> standardThresholdSet
        view == '/threshold/edit'
        model.command == command
        model.patientGroup == patientGroup
    }

    def "when updating an unknown threshold expect a redirect to last controller and action with a not found message"() {
        when:
        request.method = 'POST'
        controller.update(42)

        then:
        flash.message == "default.not.found.message"
        response.redirectedUrl == '/lastController/lastAction'
    }

    def "when updating an existing threshold with a command that has validation errors expect the edit view to be rendered with the correct model"() {
        when:
        request.method = 'POST'
        controller.update(numericThreshold.id)

        then:
        1 * controller.thresholdService.getThresholdCommandForEdit(_) >> command
        1 * controller.thresholdService.updateThreshold(command) >> { throw new ValidationException("ERROR", new MockErrors())}
        1 * controller.standardThresholdSetService.findStandardThresholdSetForThreshold(numericThreshold) >> standardThresholdSet

        view == '/threshold/edit'
        model.command == command
        model.patientGroup == patientGroup
    }

    def "when updating an existing threshold with a command without validation errors expect a redirect to last controller and action with an update message"() {
        given:
        controller.thresholdService.getThresholdCommandForEdit(_) >> command

        when:
        request.method = 'POST'
        controller.session.setAttribute('lastReferer', 'http://localhost:8080/opentele-server/standardThresholdSet/list')
        controller.update(numericThreshold.id)

        then:
        1 * controller.thresholdService.updateThreshold(command)
        flash.message == "default.updated.message"
        response.redirectedUrl == 'http://localhost:8080/opentele-server/standardThresholdSet/list'
    }

    def "when updating existing threshold values from params is bound to update command"() {
        given:
        request.method = 'POST'
        controller.session.setAttribute('lastReferer', 'http://localhost:8080/opentele-server/standardThresholdSet/list')
        controller.thresholdService.getThresholdCommandForEdit(_) >> command

        when:
        params.alertHigh = "5.5"
        params.warningHigh = "4.77"
        controller.update(numericThreshold.id)

        then:
        1 * controller.thresholdService.updateThreshold({NumericThresholdCommand cmd ->
            cmd.alertHigh == 5.5f && cmd.warningHigh == 4.77f
        })
    }
}
