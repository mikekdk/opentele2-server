package org.opentele.server.provider.integration.sms

import grails.test.mixin.TestFor
import org.opentele.server.provider.Exception.InvalidSmsIntegrationConfigurationException
import spock.lang.Specification

@TestFor(TextMessagesService)
class TextMessagesServiceSpec extends Specification {

    def setup() {
        grailsApplication.config.sms.enabled = 'true'
        grailsApplication.config.sms.gateway = 'http://www.somegateway.dk'
    }

    def "if sms integration is disabled exception is thrown"() {
        given:
        grailsApplication.config.sms.enabled = false

        when:
        service.send("12345678", "text")

        then:
        thrown(InvalidSmsIntegrationConfigurationException)
    }

    def "if sms integration is not configured with mal formed url exception is thrown"() {
        given:
        grailsApplication.config.sms.gateway = 'bla'

        when:
        service.send("12345678", "text")

        then:
        thrown(InvalidSmsIntegrationConfigurationException)
    }
}
