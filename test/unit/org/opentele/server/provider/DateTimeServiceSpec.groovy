package org.opentele.server.provider


import grails.test.mixin.*
import spock.lang.Specification

@TestFor(DateTimeService)
class DateTimeServiceSpec extends Specification{

    def "when locale is en-US 12 hour clock is used"() {
        given:
        def locale = Locale.forLanguageTag("en-US")

        when:
        def is12HourClock = service.uses12HourClock(locale)

        then:
        is12HourClock == true
    }

    def "when locale is da-DK 24 hour clock is used"() {
        given:
        def locale = Locale.forLanguageTag("da-DK")

        when:
        def is12HourClock = service.uses12HourClock(locale)

        then:
        is12HourClock == false
    }
}
