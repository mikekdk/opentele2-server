package org.opentele.server.provider

import org.opentele.server.core.test.AbstractIntegrationSpec
import org.opentele.server.model.Link
import org.opentele.server.model.LinksCategory
import org.opentele.server.model.PatientGroup

class LinksServiceIntegrationSpec extends AbstractIntegrationSpec {
    PatientGroup patientGroup1
    PatientGroup patientGroup2

    LinksService linksService

    def setup() {
        patientGroup1 = PatientGroup.list().first()
        patientGroup2 = PatientGroup.list().last()
    }

    def "can update existing links category"() {
        given:
        def created = createLinksCategory()
        def oldLinkId = created.links[0].id
        def oldPatientGroupId = created.patientGroups[0].id
        def updatedLinks = [[title: 'updated title', url: 'http://www.jp.dk']]
        def updatedPatientGroupIds = [patientGroup2.id]

        when:
        def updated = linksService.updateCategory(created.id, created.version, 'updated name', updatedLinks, updatedPatientGroupIds)

        then:
        updated.hasErrors() == false
        def loaded = LinksCategory.get(updated.id)
        loaded.name == 'updated name'
        loaded.links.size() == 1
        loaded.links[0].title == "updated title"
        loaded.links[0].url == "http://www.jp.dk"
        loaded.patientGroups.size() == 1
        loaded.patientGroups[0].id == patientGroup2.id

        Link.get(oldLinkId) == null
        PatientGroup.get(oldPatientGroupId) != null
    }

    def "update changes are not saved when validation fails"() {
        given:
        def created = createLinksCategory()
        def updatedLinks = [] // triggers error
        def updatedPatientGroupIds = [patientGroup2.id]

        when:
        def notUpdated = linksService.updateCategory(created.id, created.version, 'updated name', updatedLinks, updatedPatientGroupIds)

        then:
        notUpdated.hasErrors() == true
    }

    def "can delete links category"() {
        given:
        def toDelete = createLinksCategory()
        def categoryId = toDelete.id
        def linkId = toDelete.links[0].id
        def patientGroupId = toDelete.patientGroups[0].id

        when:
        linksService.deleteCategory(categoryId)

        then:
        LinksCategory.withNewSession {
            LinksCategory.get(categoryId) == null
            Link.get(linkId) == null
            PatientGroup.get(patientGroupId) != null
        }
    }

    private def createLinksCategory() {
        def category = new LinksCategory(name: "name")
        category.addToLinks(new Link(title: 'link title', url: 'http://www.bt.dk'))
        category.addToPatientGroups(patientGroup1)

        return category.save(failOnError: true, flush: true)
    }
}
