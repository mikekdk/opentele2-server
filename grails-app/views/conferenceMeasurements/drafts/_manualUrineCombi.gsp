<%@ page import="org.opentele.server.core.model.types.ProteinValue" %>
<%@ page import="org.opentele.server.core.model.types.GlucoseInUrineValue" %>
<%@ page import="org.opentele.server.core.model.types.BloodInUrineValue" %>
<%@ page import="org.opentele.server.core.model.types.NitriteInUrineValue" %>
<%@ page import="org.opentele.server.core.model.types.LeukocytesInUrineValue" %>

<tmpl:drafts/measurement measurement="${measurement}"
                         headline="${g.message(code:'conferenceMeasurement.urineCombi.manual.title')}">

    <tmpl:drafts/manualSelect
            name="protein"
            title="${g.message(code: 'conferenceMeasurement.urineCombi.protein.title')}"
            options="${ProteinValue.values()}">
    </tmpl:drafts/manualSelect>

    <tmpl:drafts/manualSelect
            name="urineGlucose"
            title="${g.message(code: 'conferenceMeasurement.urineCombi.urineGlucose.title')}"
            options="${GlucoseInUrineValue.values()}">
    </tmpl:drafts/manualSelect>

    <tmpl:drafts/manualSelect
            name="urineBlood"
            title="${g.message(code: 'conferenceMeasurement.urineCombi.urineBlood.title')}"
            options="${BloodInUrineValue.values()}">
    </tmpl:drafts/manualSelect>

    <tmpl:drafts/manualSelect
            name="urineNitrite"
            title="${g.message(code: 'conferenceMeasurement.urineCombi.urineNitrite.title')}"
            options="${NitriteInUrineValue.values()}">
    </tmpl:drafts/manualSelect>

    <tmpl:drafts/manualSelect
            name="urineLeukocytes"
            title="${g.message(code: 'conferenceMeasurement.urineCombi.urineLeukocytes.title')}"
            options="${LeukocytesInUrineValue.values()}">
    </tmpl:drafts/manualSelect>

</tmpl:drafts/measurement>