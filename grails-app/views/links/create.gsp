<%@ page import="org.opentele.server.model.LinksCategory" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="default.create.label" args="[g.message(code:'linksCategory.label')]" /></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'links_style.css')}" type="text/css">
</head>
<body>
<div id="create-links" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[g.message(code:'linksCategory.name.label')]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${category}">
        <ul class="errors" role="alert">
            <g:eachError bean="${category}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form action="save" >
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <fieldset class="buttons">
            <g:hiddenField name="links" value="${links}"/>
            <g:submitButton id="saveButton" name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
